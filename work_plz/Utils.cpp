#include "Utils.h"

Utils::Utils()
{
}

double Utils::dRand()
{
	return (double)rand();
}

double Utils::dRand(double min, double max)
{
	double f = (double)rand() / RAND_MAX;
	return min + f * (max - min);
}

//max is in the range
int Utils::iRand(int min, int max) {
	return min + rand() % ((max + 1) - min);
}

double Utils::sumOfDoubleArray(double * arrHead, int size)
{
	double result = 0;

	for (int i = 0; i < size; i++)
	{
		result += arrHead[i];
	}

	return result;
}

std::vector<double> Utils::sumOfDoubleArrayWithSeparatorArray(double arr[], int divideArr[], int amount) {
	std::vector<double> result;
	int gPos = 0;
	int sum = 0;
	
	for (int i = 1; i < SUBDIMENSION_COUNT; i++)
	{
		int count = divideArr[i];
		sum = 0;

		for (int j = gPos; j < count; j++)
		{
			if (arr[j] == INFINITY) {
				sum = 1;
				break;
			}
			else {
				sum += arr[j];
			}
		}

		gPos = count;
		result.push_back(sum);
	}

	sum = 0;

	for (int i = gPos; i < amount; i++)
	{
		sum += arr[i];
	}

	result.push_back(sum);

	return result;
}


Utils::~Utils()
{
}
