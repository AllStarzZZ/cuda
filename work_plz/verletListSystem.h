#pragma once

#include "Constants.h"
#include "verletList.h"

class verletListSystem
{
public:
	verletList* getCorrespondingList(v3 pos);
	v3 getListIndexes(v3 pos);
	std::vector<int> getIDsFromSubDimension(int3 startPoint, int range);

	// 10*10*10
	verletList verletLists[VERLET_LIST_SIZE][VERLET_LIST_SIZE][VERLET_LIST_SIZE];

	verletListSystem();
	~verletListSystem();
};
