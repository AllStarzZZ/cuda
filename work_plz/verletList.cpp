#include "verletList.h"

verletList::verletList()
{
}

verletList::verletList(v3 lFront)
{
	leftFront = lFront;
	//edge size
	edgeSize = VERLET_LIST_BOUNDARY_SIZE;
	//current item count, can't use as an index
	count = 0;
	isFull = false;
}

bool verletList::isContains(particle & agent)
{
	v3 pos = agent.getPosition();

	return (pos.x >= leftFront.x && pos.x < leftFront.x + edgeSize &&
		pos.y >= leftFront.y && pos.y < leftFront.y + edgeSize &&
		pos.z >= leftFront.z && pos.z < leftFront.z + edgeSize);
}

bool verletList::insert(particle & agent)
{
	if (count < VERLET_LIST_CAPACITY - 1 && isContains(agent)) {
		particleIDs[count] = agent.getID();
		count++;
		return true;
	}
	else if (count == VERLET_LIST_CAPACITY - 1) {
		isFull == true;
		return false;
	}
	else {
		printf("Insert out of boundary");
	}

	return false;
}

bool verletList::remove(particle & agent)
{
	if (!isContains(agent))
		return false;
	else {
		int id = agent.getID();
		for (int i = 0; i < count; i++)
		{
			if (particleIDs[i] == id) {
				particleIDs[i] = particleIDs[--count];
				if (isFull) isFull = !isFull;
				return true;
			}
		}
	}

	return false;
}

bool verletList::removeById(const int id)
{
	for (int i = 0; i < count; i++)
	{
		if (particleIDs[i] == id) {
			particleIDs[i] = particleIDs[--count];
			if (isFull) isFull = !isFull;
			return true;
		}
	}
	return false;
}

verletList::~verletList()
{}
