#include "v3.h"

void v3::randomize()
{
	x = (float)rand() / (float)RAND_MAX;
	y = (float)rand() / (float)RAND_MAX;
	z = (float)rand() / (float)RAND_MAX;
}

v3::v3()
{
	randomize();
}

v3::v3(float xIn, float yIn, float zIn) {
	x = xIn;
	y = yIn;
	z = zIn;
}

float3 v3::getAsFloat3() {
	float3 f;

	f.x = x;
	f.y = y;
	f.z = z;

	return f;
}

v3::~v3()
{
}

//replaced with isImVerletList
bool operator==(v3 & a, v3 & b)
{
	int ax = a.x / VERLET_LIST_SIZE;
	int ay = a.y / VERLET_LIST_SIZE;
	int az = a.z / VERLET_LIST_SIZE;

	int bx = b.x / VERLET_LIST_SIZE;
	int by = b.y / VERLET_LIST_SIZE;
	int bz = b.z / VERLET_LIST_SIZE;

	return (ax == bx && ay == by && az == bz);
}
