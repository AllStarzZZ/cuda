#pragma once

#include <vector>

#include "v3.h"
#include "Constants.h"
#include "particle.h"

class verletList
{
public:
	verletList();
	verletList(v3 lFront);

	bool isContains(particle & agent);
	bool insert(particle & agent);
	bool remove(particle & agent);
	bool removeById(const int id);

	int edgeSize;
	int count;
	bool isFull;
	v3 leftFront;

	//store ids
	int particleIDs[VERLET_LIST_CAPACITY];

	~verletList();
};

