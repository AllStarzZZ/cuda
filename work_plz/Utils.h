#pragma once

#include <stdlib.h>
#include <vector>

#include "Constants.h"

class Utils
{
public:
	Utils();
	static double dRand();
	static double dRand(double min, double max);
	static int iRand(int min, int max);
	static double sumOfDoubleArray(double * arrHead, int size);
	static std::vector<double> sumOfDoubleArrayWithSeparatorArray(double arr[], int divideArr[], int amount);
	~Utils();
};

