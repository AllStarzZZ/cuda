#include "verletListSystem.h"

verletListSystem::verletListSystem()
{
	for (size_t i = 0; i < VERLET_LIST_SIZE; i++)
	{
		for (size_t j = 0; j < VERLET_LIST_SIZE; j++)
		{
			for (size_t k = 0; k < VERLET_LIST_SIZE; k++)
			{
				v3 startPoint = v3(
					i * VERLET_LIST_SIZE,
					j * VERLET_LIST_SIZE,
					k * VERLET_LIST_SIZE);

				verletLists[i][j][k] = verletList(startPoint);
			}
		}
	}
}

verletList * verletListSystem::getCorrespondingList(v3 pos)
{
	int x = pos.x / VERLET_LIST_SIZE;
	int y = pos.y / VERLET_LIST_SIZE;
	int z = pos.z / VERLET_LIST_SIZE;

	return &verletLists[x][y][z];
}

v3 verletListSystem::getListIndexes(v3 pos)
{
	return v3(
		pos.x / VERLET_LIST_SIZE,
		pos.y / VERLET_LIST_SIZE,
		pos.z / VERLET_LIST_SIZE);
}

std::vector<int> verletListSystem::getIDsFromSubDimension(int3 startPoint, int range)
{
	std::vector<int> result = std::vector<int>();

	for (int i = startPoint.x / VERLET_LIST_SIZE; i < startPoint.x / VERLET_LIST_SIZE + range; i++)
	{
		for (int j = startPoint.y / VERLET_LIST_SIZE; j < startPoint.y / VERLET_LIST_SIZE + range; j++)
		{
			for (int k = startPoint.z / VERLET_LIST_SIZE; k < startPoint.z / VERLET_LIST_SIZE + range; k++)
			{
				verletList * theList = &verletLists[i][j][k];

				for (int l = 0; l < theList->count; l++)
				{
					result.push_back(theList->particleIDs[l]);
				}
			}
		}
	}

	return result;
}


verletListSystem::~verletListSystem()
{
}
