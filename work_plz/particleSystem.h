#ifndef particlesystem_h
#define particlesystem_h

#pragma once

#include <iostream>
#include <vector>
#include <stdlib.h>
#include <fstream>
#include <sstream>

#define _USE_MATH_DEFINES
#include <math.h>

#include "particle.h"
#include "Constants.h"
#include "v3.h"
#include "Utils.h"
#include "verletListSystem.h"


class particleSystem
{
public:
	particleSystem();
	particleSystem(unsigned int numParticles, bool initFromFile);
	~particleSystem();

	void initFromFile();

	void printParticles();
	void printParticlcesArrays(float* p, float* v, float* a);
	std::vector<particle> particleSystem::getParticlesVector();
	float* particlesPosfloatArray();
	float3* particlesPosAsFloat3Array();
	float* particlesVelfloatArray();
	float* particlesAccfloatArray();
	void printPosFloatArray(float* posFloatArray);
	void printVelFloatArray(float* velFloatArray);
	void printAccFloatArray(float* accFloatArray);
	void gravitySerial(unsigned int simulationLength);

	void displacement(particle* agent);
	void displacement8(std::vector<particle*> particles);
	
	//parallel consideration (comment out if CUDA doesn't work)
	void gravityBoth(float3* positions, float3* velocities, float3* accelerations, unsigned int numRounds);
	bool isSame(float3* positions, float3* velocities, float3* accelerations);

	//particle functionatilies
	particle * getRandomParticle();
	//give back 8 IDs
	int * getParticleIDsFromEightDimensions();
	//give back 8 particles, the parameter 8 * int long
	std::vector<particle*> getParticlesBasedOnIDs(int * ids);

	v3 getRandomUnitVector() const;
	std::vector<v3> getNeighborsParticlesPosition(particle& target);
	std::vector<v3> getNeighborsParticlesPosition(std::vector<particle*> particles, int * divideIndicies);
	void updateParticleWhenMove(particle * p);
	void updateParticleWhenMove8(particle * p, int index);
	bool isInASameVerletList(v3& a, v3& b);

	void rollbackDisplacement(particle * p);
	void rollbackDisplacement8(particle * p, int index);
	void evaulateMove(particle * p, double energyAfterMove);
	void evaulateMove(particle * p, int index, double energyAfterMove, double energyBeforeMove);

	//particles main container
	std::vector<particle> particles;
	verletListSystem* verletSys;

	//store the old pos before the displacement
	v3 particleOldPosition;
	std::vector<v3> particlesOldPositions;

	float* posFloatArrayPtr;
	float* velFloatArrayPtr;
	float* accFloatArrayPtr;

	bool particlesPosFloatArrayCalled;
	bool particlesVelFloatArrayCalled;
	bool particlesAccFloatArrayCalled;

	unsigned int systemIteration;
};

#endif