#include "particleSystem.h"

particleSystem::particleSystem()
{
	srand((unsigned int)time(0));
	particlesPosFloatArrayCalled = false;
	particlesVelFloatArrayCalled = false;
	particlesAccFloatArrayCalled = false;
	systemIteration = 0;
}

particleSystem::particleSystem(unsigned int numParticles, bool initFromFile)
{
	v3 zero = { 0.0, 0.0, 0.0 };
	srand((unsigned int)time(0));
	verletSys = new verletListSystem();

	if (initFromFile) {
		this->initFromFile();
	}
	else {
		unsigned int i;
		//generate agents
		for (i = 0; i < numParticles; i++) {
			particle p;
			p.setID(i);
			//p.randomPosition(0.0, (float)(WORLD_DIM)/4);
			p.randomPositivePosition(0.0, (float)(WORLD_DIM));
			p.setVelocity(zero);
			p.setAcceleration(zero);
			p.setMass((float)UNIVERSAL_MASS);
			this->particles.push_back(p);
		}
	}

	//store them in the verlet system
	for (std::vector<particle>::iterator it = particles.begin(); it != particles.end(); ++it) {
		v3 * pos = &it->getPosition();
		verletList* list = verletSys->getCorrespondingList(*pos);

		if (!list->insert(*it))
			printf("szaaaaaaaaaaaaaaaaaaaaaaaaaaaar");
	}
}

void particleSystem::initFromFile()
{
	int id = 0;
	v3 zero = { 0.0, 0.0, 0.0 };

	std::ifstream inFile("seed.csv");
	std::string line;
	std::getline(inFile, line); // skip SEP=; line

	while (std::getline(inFile, line))
	{
		v3 particlePos;

		std::stringstream ss(line);
		ss >> particlePos.x;

		ss.seekg((int)ss.tellg() + 1); // skip separator
		ss >> particlePos.y;

		ss.seekg((int)ss.tellg() + 1); // skip separator
		ss >> particlePos.z;

		particle p = particle(particlePos);
		p.setID(id);
		p.setVelocity(zero);
		p.setAcceleration(zero);
		p.setMass((float)UNIVERSAL_MASS);
		this->particles.push_back(p);

		id++;
	}
}

void particleSystem::printParticles() {
	for (std::vector<particle>::iterator it = particles.begin(); it != particles.end(); ++it) {
		it->printProps();
	}
}

void particleSystem::printParticlcesArrays(float* p, float* v, float* a) {
	unsigned int i;
	for (i = 0; i < NUM_PARTICLES * 3; i += 3) {
		printf("id: %d\tpos: (%f, %f, %f)\tvel: (%f, %f, %f)\tacc:(%f, %f, %f)\n", i/3, p[i], p[i + 1], p[i + 2], v[i], v[i + 1], v[i + 2], a[i], a[i + 1], a[i + 2]);
	}
}

std::vector<particle> particleSystem::getParticlesVector() {
	return particles;
}

void particleSystem::gravitySerial(unsigned int simulationLength) {

	if (SERIAL_DEBUG){
		for (std::vector<particle>::iterator it = particles.begin(); it != particles.end(); ++it) {
			std::cout << "import - ";
			it->printProps();
		}
	}

	std::vector<particle> temp; //store the old (the read source), particles - class member - is where the updates occur
	//reason: no particle should update unless all compuation for all particles are finished

	unsigned int counter = 0;
	while (counter < simulationLength) {
		temp = particles;
		for (unsigned int i = 0; i < temp.size(); i++) {
			v3 force = v3(0.0, 0.0, 0.0);
			for (unsigned int j = 0; j < temp.size(); j++) {
				if (i != j) { // force on i (it) by j (itt)
					v3 currRay = temp[i].getRay(temp[j]);
					if (SERIAL_DEBUG) {
						printf("ray (%u,%u); (%f,%f,%f)\n", i, j, currRay.x, currRay.y, currRay.z);
					}
					float dist = temp[i].getDistance(temp[j]);
					if (SERIAL_DEBUG) {
						printf("distance (%u,%u); %f\n", i, j, dist);
					}
					float mi = temp[i].getMass();
					float mj = temp[j].getMass();
					float xadd =(float)GRAVITY * (float)mj * (float)currRay.x / (float)pow(dist, 2.0);
					float yadd = (float)GRAVITY * (float)mj * (float)currRay.y / (float)pow(dist, 2.0);
					float zadd = (float)GRAVITY * (float)mj * (float)currRay.z / (float)pow(dist, 2.0);
					if (SERIAL_DEBUG) {
						printf("(xadd, yadd, zadd) (%u,%u); (%f,%f,%f)\n", i, j, xadd, yadd, zadd);
					}
					force.x -= xadd/float(mi); // F=ma --> a=F/m
					force.y -= yadd/float(mi);
					force.z -= zadd/float(mi);
				}
			}
			//it->updateParticle(EPOCH, force);
			particles[i].updateParticle(EPOCH, force); //KEY: update occurs at the class member particles vector
			if (SERIAL_UPDATE_OUTPUT && (i == 0 || i == 299)) {
				std::cout << "update (" << particles[i].id << "): ";
				particles[i].printProps();
			}
		}
		counter++;
	}
	systemIteration += simulationLength;
}

void particleSystem::displacement(particle * agent)
{
	particleOldPosition = agent->getPosition();

	//displacement vec
	v3 dv = getRandomUnitVector();

	double vLength = agent->radius_mean * agent->mobility_ratio * Utils::dRand(0.0, 1.0);

	v3 newPos = v3(
		particleOldPosition.x + dv.x * vLength,
		particleOldPosition.y + dv.y * vLength,
		particleOldPosition.z + dv.z * vLength);
	v3 lIndex = verletSys->getListIndexes(newPos);

	if (lIndex.x >= 0 && lIndex.x < VERLET_LIST_SIZE &&
		lIndex.y >= 0 && lIndex.y < VERLET_LIST_SIZE &&
		lIndex.z >= 0 && lIndex.z < VERLET_LIST_SIZE) {

		agent->setPosition(newPos);
		//for that case if move another boundary
		updateParticleWhenMove(agent);
	}
	else {
		printf("Displacement failed! --- out of boundary");
	}
}

void particleSystem::displacement8(std::vector<particle*> particles)
{
	int index = 0;
	for each (particle* p in particles)
	{
		particlesOldPositions.push_back(p->getPosition());

		//displacement vec
		v3 dv = getRandomUnitVector();

		double vLength = p->radius_mean * p->mobility_ratio * Utils::dRand(0.0, 1.0);

		v3 newPos = v3(
			particlesOldPositions[index].x + dv.x * vLength,
			particlesOldPositions[index].y + dv.y * vLength,
			particlesOldPositions[index].z + dv.z * vLength);
		v3 lIndex = verletSys->getListIndexes(newPos);

		if (lIndex.x >= 0 && lIndex.x < VERLET_LIST_SIZE &&
			lIndex.y >= 0 && lIndex.y < VERLET_LIST_SIZE &&
			lIndex.z >= 0 && lIndex.z < VERLET_LIST_SIZE) {

			p->setPosition(newPos);
			//for that case if move another boundary
			updateParticleWhenMove8(p, index);
		}
		else {
			printf("Displacement failed! --- out of boundary");
		}

		index++;
	}

	if(DEBUG) printf("%d displacment happend!", index);
}

// Marsaglia random 
v3 particleSystem::getRandomUnitVector() const {
	float x1, x2;

	float x12, x22;
	do {
		x1 = Utils::dRand(-1.0, 1.0);
		x2 = Utils::dRand(-1.0, 1.0);

		x12 = x1*x1;
		x22 = x2*x2;

	} while (x12 + x22 >= 1.0);

	float x = 2 * x1 * sqrt(1 - x12 - x22);
	float y = 2 * x2 * sqrt(1 - x12 - x22);
	float z = 1 - 2 * (x12 + x22);

	return v3(x, y, z);
}

float* particleSystem::particlesPosfloatArray() {
	particlesPosFloatArrayCalled = true; //for destructor
	float* posArray = new float[NUM_PARTICLES * 3];
	unsigned int i = 0;
	for (std::vector<particle>::iterator it = particles.begin(); it != particles.end(); ++it) {
		posArray[i] = it->getPosition().x;
		posArray[i + 1] = it->getPosition().y;
		posArray[i + 2] = it->getPosition().z;
		i += 3;
	}
	posFloatArrayPtr = posArray; //for destructor
	return posArray;
}

float3 * particleSystem::particlesPosAsFloat3Array()
{
	return nullptr;
}

float* particleSystem::particlesVelfloatArray() {
	particlesVelFloatArrayCalled = true; //for destructor
	float* velArray = new float[NUM_PARTICLES * 3];
	unsigned int i = 0;
	for (std::vector<particle>::iterator it = particles.begin(); it != particles.end(); ++it) {
		velArray[i] = it->getVelocity().x;
		velArray[i + 1] = it->getVelocity().y;
		velArray[i + 2] = it->getVelocity().z;
		i += 3;
	}
	velFloatArrayPtr = velArray; //for destructor
	return velArray;
}


float* particleSystem::particlesAccfloatArray() {
	particlesAccFloatArrayCalled = true; //for destructor
	float* accArray = new float[NUM_PARTICLES * 3];
	unsigned int i = 0;
	for (std::vector<particle>::iterator it = particles.begin(); it != particles.end(); ++it) {
		accArray[i] = it->getAcceleration().x;
		accArray[i + 1] = it->getAcceleration().y;
		accArray[i + 2] = it->getAcceleration().z;
		i += 3;
	}
	accFloatArrayPtr = accArray; //for destructor
	return accArray;
}

void particleSystem::printPosFloatArray(float* posFloatArray) {
	float x, y, z;
	unsigned int i;
	for (i = 0; i < NUM_PARTICLES * 3; i+=3) {
		x = posFloatArray[i];
		y = posFloatArray[i + 1];
		z = posFloatArray[i + 2];
		printf("id: %d\tpos: (%f, %f, %f)\n", i/3, x, y, z);
	}
}

void particleSystem::printVelFloatArray(float* velFloatArray) {
	float x, y, z;
	unsigned int i;
	for (i = 0; i < NUM_PARTICLES * 3; i += 3) {
		x = velFloatArray[i];
		y = velFloatArray[i + 1];
		z = velFloatArray[i + 2];
		printf("id: %d\tvel: (%f, %f, %f)\n", i / 3, x, y, z);
	}
}

void particleSystem::printAccFloatArray(float* accFloatArray) {
	float x, y, z;
	unsigned int i;
	for (i = 0; i < NUM_PARTICLES * 3; i += 3) {
		x = accFloatArray[i];
		y = accFloatArray[i + 1];
		z = accFloatArray[i + 2];
		printf("id: %d\tacc: (%f, %f, %f)\n", i / 3, x, y, z);
	}
}

//comment out if CUDA doesn't work
bool particleSystem::isSame(float3* p, float3* v, float3* a) {
	bool retval = true;
	for (unsigned int i = 0; i < NUM_PARTICLES; i++) {
		v3 pos = particles[i].getPosition();
		v3 vel = particles[i].getVelocity();
		v3 acc = particles[i].getAcceleration();

		bool currSame = (pos.x == p[i].x && pos.y == p[i].y && pos.z == p[i].z
			&& vel.x == v[i].x && vel.y == v[i].y && v[i].z
			&& acc.x == a[i].x && acc.y == a[i].y && acc.z == a[i].z);

		if (retval && !currSame) { //first difference
			std::cout << "NOT the same, differences below..." << std::endl;
		}
		if (!currSame) {
			retval = false;
			std::cout << "(serial) ";
			particles[i].printProps();
			printf("(parallel) - id: %d\tpos: (%f, %f, %f)\tvel: (%f, %f, %f)\tacc:(%f, %f, %f)\n", i / 3, p[i].x, p[i].y, p[i].z, v[i].x, v[i].y, v[i].z, a[i].x, a[i].y, a[i].z);
		}
	}
	if (retval) std::cout << "SAME\n" << std::endl;
	std::cout << std::endl;
	return retval;
}

particle * particleSystem::getRandomParticle()
{
	int vectorSize = (int)particles.size();
	int rndIndex = Utils::iRand(0, vectorSize - 1);
	particle * result = &particles[rndIndex];

	return result;
}

int * particleSystem::getParticleIDsFromEightDimensions()
{
	int const dividedDimensions = SUBDIMENSION_COUNT;
	int * result = (int *)malloc(sizeof(int) * dividedDimensions);
	std::memset(result, -1, sizeof(int) * dividedDimensions);

	int3 startPositions[dividedDimensions];

	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			for (int k = 0; k < 2; k++)
			{
				int index = i * pow(2, 2) + j * 2 + k;
				int3 pos = int3();
				pos.x = i * WORLD_DIM / 2;
				pos.y = j * WORLD_DIM / 2;
				pos.z = k * WORLD_DIM / 2;

				startPositions[index] = pos;
			}
		}
	}

	for (int i = 0; i < dividedDimensions; i++)
	{
		std::vector<int> particlesvec = 
			verletSys->getIDsFromSubDimension(startPositions[i], VERLET_LIST_BOUNDARY_SIZE / 2);

		int size = particlesvec.size();
		if(size > 0) result[i] = particlesvec[Utils::iRand(0, size - 1)];
	}

	return result;
}

std::vector<particle*> particleSystem::getParticlesBasedOnIDs(int * ids)
{
	std::vector<particle *> result = std::vector<particle *>();

	//int * ids = getParticleIDsFromEightDimensions();
	for (int i = 0; i < SUBDIMENSION_COUNT; i++)
	{
		result.push_back(&particles[ids[i]]);
	}

	return result;
}


bool IsIndexValid(const int x, const int y, const int z) {
	return x >= 0 && x < VERLET_LIST_SIZE &&
		y >= 0 && y < VERLET_LIST_SIZE &&
		z >= 0 && z < VERLET_LIST_SIZE;
}

std::vector<v3> particleSystem::getNeighborsParticlesPosition(particle &target) {
	std::vector<v3> positions = std::vector<v3>();
	v3 agentPos = target.pos;
	v3 listIdx = v3(
		agentPos.x / VERLET_LIST_SIZE,
		agentPos.y / VERLET_LIST_SIZE,
		agentPos.z / VERLET_LIST_SIZE);

	for (int i = -1; i < 2; i++)
	{
		for (int j = -1; j < 2; j++)
		{
			for (int k = -1; k < 2; k++)
			{
				int x = listIdx.x + i;
				int y = listIdx.y + j;
				int z = listIdx.z + k;

				if (IsIndexValid(x, y, z)) {

					verletList * theList = &verletSys->verletLists[x][y][z];

					for (int l = 0; l < theList->count; l++)
					{
						if (target.getID() != theList->particleIDs[l]) {
							particle * tmp = &particles[theList->particleIDs[l]];
							positions.push_back(tmp->pos);
						}
					}
				}
			}
		}
	}

	return positions;
}

std::vector<v3> particleSystem::getNeighborsParticlesPosition(
	std::vector<particle*> particles,
	int * divideIndicies)
{
	std::vector<v3> result = std::vector<v3>();

	for (int i = 0; i < particles.size(); i++)
	{
		std::vector<v3> positions = getNeighborsParticlesPosition(*particles[i]);
		divideIndicies[i] = result.size();
		for (int j = 0; j < positions.size(); j++)
		{
			result.push_back(positions[j]);
		}
	}

	return result;
}

//p has the changed position
void particleSystem::updateParticleWhenMove(particle * p)
{
	v3 tmpPos = p->getPosition();
	if (!isInASameVerletList(particleOldPosition, tmpPos)) {
		verletSys->getCorrespondingList(particleOldPosition)->remove(*p);
		verletSys->getCorrespondingList(tmpPos)->insert(*p);
	}
}

void particleSystem::updateParticleWhenMove8(particle * p, int index)
{
	v3 tmpPos = p->getPosition();
	if (!isInASameVerletList(particlesOldPositions[index], tmpPos)) {
		verletSys->getCorrespondingList(particlesOldPositions[index])->remove(*p);
		verletSys->getCorrespondingList(tmpPos)->insert(*p);
	}
}

bool particleSystem::isInASameVerletList(v3 & a, v3 & b)
{
	int ax = a.x / VERLET_LIST_SIZE;
	int ay = a.y / VERLET_LIST_SIZE;
	int az = a.z / VERLET_LIST_SIZE;

	int bx = b.x / VERLET_LIST_SIZE;
	int by = b.y / VERLET_LIST_SIZE;
	int bz = b.z / VERLET_LIST_SIZE;

	return (ax == bx && ay == by && az == bz);
}

void particleSystem::rollbackDisplacement(particle * p)
{
	v3 tmpPos = p->getPosition();
	p->setPosition(particleOldPosition);
	if (!isInASameVerletList(particleOldPosition, tmpPos)) {
		verletSys->getCorrespondingList(tmpPos)->remove(*p);
		verletSys->getCorrespondingList(particleOldPosition)->insert(*p);
	}
}

void particleSystem::rollbackDisplacement8(particle * p, int index)
{
	v3 tmpPos = p->getPosition();
	p->setPosition(particlesOldPositions[index]);
	if (!isInASameVerletList(particlesOldPositions[index], tmpPos)) {
		verletSys->getCorrespondingList(tmpPos)->remove(*p);
		verletSys->getCorrespondingList(particlesOldPositions[index])->insert(*p);
	}
}

void particleSystem::evaulateMove(particle * p, double energyAfterMove)
{
	double energyBeforeMove = p->getEnergy();
	if (energyAfterMove < energyBeforeMove) {
		//OK
		p->setEnergy(energyAfterMove);
	}
	else if (Utils::dRand(0.0, 1.0) < std::exp(-(energyAfterMove - energyBeforeMove))) {
		//OK
		p->setEnergy(energyAfterMove);
	}
	else {
		//Reject
		rollbackDisplacement(p);
	}
}

void particleSystem::evaulateMove(particle * p, int index, double energyAfterMove, double energyBeforeMove)
{
	if (energyAfterMove < energyBeforeMove) {
		//OK
		p->setEnergy(energyAfterMove);
	}
	else if (Utils::dRand(0.0, 1.0) < std::exp(-(energyAfterMove - energyBeforeMove))) {
		//OK
		p->setEnergy(energyAfterMove);
	}
	else {
		//Reject
		rollbackDisplacement8(p, index);
		if (DEBUG) printf("Rollback happend at index: %d", index);
	}
}

particleSystem::~particleSystem()
{
	delete[] posFloatArrayPtr;
	delete[] velFloatArrayPtr;
	delete[] accFloatArrayPtr;

	delete verletSys;
}