#ifndef __v3_h__
#define __v3_h__

#include <math.h>
#include <stdlib.h>
#include <vector_types.h>

#include "Constants.h"

#pragma once
class v3
{
public:

	float x;
	float y;
	float z;

	v3();
	v3(float xIn, float yIn, float zIn);
	float3 getAsFloat3();
	void randomize();

	friend
	bool operator== (v3& a, v3& b);
	~v3();
};

#endif